#!/bin/bash

total_linhas=0

for arquivo in "$@"; do
        num_linhas=$(wc -l < "$arquivo")
        total_linhas=$((total_linhas + num_linhas))
done

echo "Total de linhas: $total_linhas"

