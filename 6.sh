#!/bin/bash

opcao=0
arquivo=""

while [ "$opcao" != "5" ]; do
    echo "1 - Digitar o nome de um arquivo"
    echo "2 - Criar o arquivo com nome"
    echo "3 - Adicionar um número ao final do arquivo"
    echo "4 - Remover o arquivo"
    echo "5 - Exibir na tela o arquivo"
    echo "6 - Sair"
    read -p "Escolha uma opção: " opcao

    case $opcao in
        1)
            read -p "Digite o nome do arquivo: " arquivo
            ;;
        2)
            if [ -z "$arquivo" ]; then
                echo "Nome do arquivo não especificado. Digite a opção 1 para informar o nome."
            else
                touch "$arquivo"
                echo "Arquivo criado: $arquivo"
            fi
            ;;
        3)
            if [ -z "$arquivo" ]; then
                echo "Nome do arquivo não especificado. Digite a opção 1 para informar o nome."
            else
                while ! [[ "$numero" =~ ^[0-9]+$ ]]; do
                    read -p "Digite o número a ser adicionado ao final do arquivo: " numero
                    if ! [[ "$numero" =~ ^[0-9]+$ ]]; then
                        echo "Digite apenas números."
                    fi
                done
                echo "$numero" >> "$arquivo"
                echo "Número adicionado ao arquivo: $numero"
                numero=""
            fi
            ;;
        4)
            if [ -z "$arquivo" ]; then
                echo "Nome do arquivo não especificado. Digite a opção 1 para informar o nome."
            else
                rm "$arquivo"
                echo "Arquivo removido: $arquivo"
                arquivo=""
            fi
            ;;
	5)
            if [ -z "$arquivo" ]; then
                echo "Nome do arquivo não especificado. Digite a opção 1 para informar o nome."
            else
                cat "$arquivo"                
            fi
            ;;       
       
        6)
            echo "Saindo..."
            ;;
        *)
            echo "Opção inválida. Digite uma opção válida de 1 a 5."
            ;;
    esac

    echo
done

