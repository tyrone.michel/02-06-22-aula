#!/bin/bash
exec > >(tee resultado.txt) 2>&1
for i in $(ls /etc/* /tmp/*); do
    [ -d "$i" ] && echo "$i é um diretório." && continue
    [ -f "$i" ] && echo "$i é um arquivo regular." && continue
    [ -L "$i" ] && echo "$i é um link simbólico." && continue
    [ -x "$i" ] && echo "$i é um executável."
done
