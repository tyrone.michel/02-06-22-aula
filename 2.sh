#!/bin/bash
exec > >(tee resultado.txt) 2>&1
# Atribui o valor do argumento à variável 'numero'
numero=$1
soma=0

# Loop para somar os números
for ((i=1; i<=numero; i++)); do
    soma=$((soma + i))
done

# Resultado
echo "A soma de todos os números de 1 até $numero é: $soma"

# Resultado.txt
echo "A soma de todos os números de 1 até $numero é: $soma" > resultado.txt

exit 0

