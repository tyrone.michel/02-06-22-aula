#!/bin/bash

# Nome do arquivo de lista de sites
arquivo_sites="sites.txt"

# Diretório onde os sites serão baixados
diretorio_destino="sites_downloads"

# Verifica se o diretório de destino existe, caso contrário cria
if [ ! -d "$diretorio_destino" ]; then
    mkdir "$diretorio_destino"
fi

# Lê cada site do arquivo de lista e faz o download
while IFS= read -r site; do
    # Remove espaços em branco do início e fim do site
    site=$(echo "$site" | sed 's/^[ \t]*//;s/[ \t]*$//')

    # Cria o diretório para o site
    nome_diretorio=$(echo "$site" | sed 's/[^A-Za-z0-9._-]/_/g')
    diretorio_site="$diretorio_destino/$nome_diretorio"
    mkdir -p "$diretorio_site"

    # Faz o download do site dentro do diretório correspondente
    wget -P "$diretorio_site" "$site"
done < "$arquivo_sites"

